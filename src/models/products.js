const connection = require('../config/mysql')

module.exports = {
    getProducts: function(){
        return new Promise(function(resolve, reject) {
            connection.query('SELECT * FROM products', function(error, result) {
                if (!error){
                    resolve(result)
                } else {
                    reject(new Error(error))
                }
            })
        })
    },
    postProduct: function(setData){
        return new Promise(function(resolve, reject) {
            connection.query('INSERT INTO products SET ?', setData, function(error, result){
                if (!error){
                    const newData = {
                        id: result.insertId,
                        ...setData
                    }

                    resolve(newData)
                } else {
                    reject(new Error(error))
                }
            })
        })
    },
    putProduct: function(id, setData) {
        return new Promise(function(resolve, reject) {
            connection.query('UPDATE products SET ? WHERE id=?', [setData, id], function(error, result){
                if (!error){
                    resolve(result)
                } else {
                    reject(new Error(error))
                }
            })

        })
    }
}