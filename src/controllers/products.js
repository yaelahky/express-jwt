const productModels = require('../models/products')
const helper = require('../helpers')

module.exports = {
    getProducts: async function(request, response){
        try {
            const result = await productModels.getProducts()
        
            return helper.response(response, 200, result)
        } catch (error) {
            return helper.response(response, 500, error)
        }
    },
    postProduct: async function(request, response){
        try {
            const setData = request.body
            const result = await productModels.postProduct(setData)

            return helper.response(response, 200, result)
        } catch (error) {
            return helper.response(response, 500, error)
        }
    },
    putProduct: async function(request, response){
        try {
            const setData = request.body
            const id = request.params.id
            const result = await productModels.putProduct(id ,setData)

            return helper.response(response, 200, result)
        } catch (error) {
            return helper.response(response, 500, error)
        }
    },
}