const express = require('express')
const Route = express.Router()

const productController = require('../controllers/products')
const {authorization} = require('../middleware/auth')

Route
    .get('/', authorization, productController.getProducts)  // / = /products
    .post('/', authorization, productController.postProduct)
    .put('/:id', productController.putProduct)
// Route.delete('/:id')

module.exports = Route