const express = require('express')
const Route = express.Router()

const productRoutes = require('./routes/products')
const authRoutes = require('./routes/auth')

Route.use('/products', productRoutes)
Route.use('/auth', authRoutes)

module.exports = Route